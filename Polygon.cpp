#include "Polygon.h"
#include <iostream>

Polygon::Polygon(const string& type, const string& name) : Shape(name, type)
{

}

Polygon::~Polygon()
{}

void Polygon::move(const Point& other)
{
	for (int i = 0; i < _points.size(); i++)
	{
		_points[i] += other;
	}
}