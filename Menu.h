#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>


class Menu
{
public:

	Menu();
	~Menu();

	bool mainMenu();
	// more functions..

private: 

	void addShape();
	void printInfo();
	void specificShape(Shape* s, int i);
	void clearAll();
	void drawAll();
	void deleteAll();
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;

	Shape** shapes;	//array of pointers to shape
	int shapeCount;
};

