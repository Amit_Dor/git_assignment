#include "Shape.h"

#include <iostream>
using namespace std;
#include <string>

Shape::Shape(const string& name, const string& type)
{
	this->_name = name;
	this->_type = type;
}

//print all the details of a shape
void Shape::printDetails() const
{
	cout << "name: " <<_name << " type: " << _type << " Area: " << getArea() << " Perimeter: " << getPerimeter() << endl;
}

string Shape::getType() const
{
	return this->_type;
}

string Shape::getName() const
{
	return this->_name;
}