#include "Point.h"
#include <cmath>

Point::Point(double x, double y)
{
	this->x = x;
	this->y = y;
}
Point::Point(const Point& other)
{
	this->x = other.x;
	this->y = other.y;
}
Point::~Point()
{}

Point Point::operator+(const Point& other) const
{
	Point p(this->x + other.x, this->y + other.y);
	return p;
}

Point& Point::operator+=(const Point& other)
{
	this->x += other.x;
	this->y += other.y;
	return *this;
}

double Point::getX() const
{
	return x;
}

double Point::getY() const
{
	return y;
}

double Point::distance(const Point& other) const
{
	return(sqrt(pow(this->x - other.x, 2) + pow(this->y - other.y, 2)));
}