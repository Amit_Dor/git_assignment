#include "Rectangle.h"

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name) : Polygon(type,name)
{
	if (length <= 0 || width <= 0)
	{
		throw invalid_argument("The length an the width cannor ne negative!");//throwing an exception
	}
	_points.push_back(a);	//adds the point to the vector
	Point p(length, width);
	_points.push_back(p);
}

myShapes::Rectangle::~Rectangle()
{
	_points.pop_back();
}

double myShapes::Rectangle::getArea() const
{
	return _points[1].getX() * _points[1].getY();
}

double myShapes::Rectangle::getPerimeter() const
{
	return 2 * (_points[1].getX() + _points[1].getY());
}

void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


