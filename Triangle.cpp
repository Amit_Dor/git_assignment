#include "Triangle.h"

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name) : Polygon(type, name)
{
	if ((a.getX() == b.getX() && b.getX() == c.getX()) || (a.getY() == b.getY() && b.getY() == c.getY()))
	{
		throw invalid_argument("The points cannot be on one line!");	//throwing an exception
	}
	else
	{
		_points.push_back(a);
		_points.push_back(b);
		_points.push_back(c);
		//adding of the points to the vector
	}
}

Triangle::~Triangle()
{
	_points.pop_back();
	_points.pop_back();
	_points.pop_back();
}

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}

double Triangle::getArea() const
{
	double s = _points[0].getX()*(_points[2].getY() - _points[1].getY()) + _points[1].getX()*(_points[0].getY() - _points[2].getY()) + _points[2].getX()*(_points[1].getY() - _points[0].getY());
	if (s < 0)
	{
		s *= -1;
	}
	s *= 0.5;
	return s;
}

double Triangle::getPerimeter() const
{
	return _points[0].distance(_points[1]) + _points[1].distance(_points[2]) + _points[2].distance(_points[0]);
}
