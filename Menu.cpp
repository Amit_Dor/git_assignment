#include "Menu.h"
#include <string>
#include <iostream>

#define MAX_NUM 50

Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
	shapes = new Shape*[MAX_NUM];
	shapeCount = 0;
}

Menu::~Menu()
{
	_disp->close();
	deleteAll();
	delete shapes;
	delete _board;
	delete _disp;
}

/*The main menu. returns false if exit was chosen and else true*/
bool Menu::mainMenu()
{
	int choice = 0, choice2 = 0;
	do
	{
		system("cls");
		cout << "Enter 0 to add a new shape." << endl;
		cout << "Enter 1 to modify or get information from a current shape." << endl;
		cout << "Enter 2 to delete all of the shapes." << endl;
		cout << "Enter 3 to exit." << endl;
		cin >> choice;
	} while (choice > 3 || choice < 0);
	switch (choice)
	{
	case 0:
		addShape();
		break;
	case 1:
		if (shapeCount != 0)//if there is a shape
		{
			do
			{
				printInfo();
				cin >> choice2;
			} while (choice2 >= shapeCount || choice2 < 0);
			specificShape(shapes[choice2], choice2);
		}
		break;
	case 2:
		deleteAll();
		break;

	case 3:
		return false;
		break;
	}

	return true;
}

/*This function adds a shape to the array*/
void Menu::addShape()
{
	int choice = 0, x = 0, y = 0, r = 0, a = 0, b = 0;
	string name;
	do
	{
		system("cls");
		cout << "Enter 0 to add a circle.\nEnter 1 to add an arrow.\nEnter 2 to add a triangle.\nEnter 3 to add a rectangle.\n";
		cin >> choice;
	} while (choice < 0 || choice > 3);

	if (choice == 0)//Circle
	{
		cout << "Please enter X:" << endl;
		cin >> x;
		cout << "Please enter Y:" << endl;
		cin >> y;
		cout << "Please enter radius:" << endl;
		cin >> r;
		cout << "Please enter the name of the shape:" << endl;
		cin >> name;
		Point p(x, y);
		shapes[shapeCount++] = new Circle(p, r, "Circle", name);
		shapes[shapeCount - 1]->draw(*_disp,*_board);
	}
	else if (choice == 1)//Arrow
	{ 
		cout << "Enter the X of point number: 1" << endl;
		cin >> x;
		cout << "Enter the Y of point number: 1" << endl;
		cin >> y;
		Point p1(x, y);
		cout << "Enter the X of point number: 2" << endl;
		cin >> x;
		cout << "Enter the Y of point number: 2" << endl;
		cin >> y;
		Point p2(x, y);
		cout << "Please enter the name of the shape:" << endl;
		cin >> name;
		shapes[shapeCount++] = new Arrow(p1, p2, "Arrow", name);
		shapes[shapeCount - 1]->draw(*_disp, *_board);
	}
	else if (choice == 2)//Tringle
	{
		cout << "Enter the X of point number: 1" << endl;
		cin >> x;
		cout << "Enter the Y of point number: 1" << endl;
		cin >> y;
		Point p1(x, y);
		cout << "Enter the X of point number: 2" << endl;
		cin >> x;
		cout << "Enter the Y of point number: 2" << endl;
		cin >> y;
		Point p2(x, y);
		cout << "Enter the X of point number: 3" << endl;
		cin >> x;
		cout << "Enter the Y of point number: 3" << endl;
		cin >> y;
		Point p3(x, y);
		cout << "Please enter the name of the shape:" << endl;
		cin >> name;
		try
		{
			shapes[shapeCount++] = new Triangle(p1, p2, p3, "Triangle", name);
			shapes[shapeCount - 1]->draw(*_disp, *_board);
		}
		catch (invalid_argument e)//catching the exception
		{
			cout << e.what() << endl;
			system("pause");
		}
	}
	else if (choice == 3)//Rectangle
	{
		cout << "Enter the X of the to left corner:" << endl;
		cin >> x;
		cout << "Enter the Y of the to left corner:" << endl;
		cin >> y;
		Point p1(x, y);
		cout << "Please enter the length of the shape:" << endl;
		cin >> a;
		cout << "Please enter the height of the shape:" << endl;
		cin >> b;
		cout << "Enter the name of the shape:" << endl;
		cin >> name;
		try
		{
			shapes[shapeCount++] = new myShapes::Rectangle(p1, a, b, "Rectangle", name);
			shapes[shapeCount - 1]->draw(*_disp, *_board);
		}
		catch (invalid_argument e)//catching the exception
		{
			cout << e.what() << endl;
			system("pause");
		}
	}
}

//Printing the shapes
void Menu::printInfo()
{
	for (int i = 0; i < shapeCount; i++)
	{
		cout << "Enter " << i << " for " << shapes[i]->getName() << "(" << shapes[i]->getType() << ")" << endl;
	}
}

//The menu for a specific shape (2 option)
void Menu::specificShape(Shape* s, int i)
{
	int choice = 0, x = 0, y = 0;
	do
	{
		cout << "Enter 0 to move the shape.\nEnter 1 to get its details.\nEnter 2 to remove the shape.\n";
		cin >> choice;
	} while (choice < 0 || choice > 2);
	if (choice == 0)//move
	{
		cout << "Please enter the X moving scale:" << endl;
		cin >> x;
		cout << "Please enter the Y moving scale:" << endl;
		cin >> y;
		Point p(x, y);
		clearAll();//clearing the screen
		s->move(p);//moving of the shape
		drawAll();//draw all the shapes again
	}
	else if (choice == 1)
	{
		s->printDetails();
		system("pause");
	}
	else
	{
		clearAll();
		delete shapes[i];
		shapes[i] = shapes[shapeCount - 1];
		shapes[shapeCount - 1] = NULL;
		shapeCount--;
		drawAll();
	}
}

//draw all the shapes
void Menu::drawAll()
{
	for (int i = 0; i < shapeCount; i++)
	{
		shapes[i]->draw(*_disp, *_board);
	}
}

//clear all the shapes from the screen
void Menu::clearAll()
{
	for (int i = 0; i < shapeCount; i++)
	{
		shapes[i]->clearDraw(*_disp, *_board);
	}
}

//delete ak the shapes from the array
void Menu::deleteAll()
{
	clearAll();
	for (int i = 0; i < shapeCount; i++)
	{
		delete shapes[i];
		shapes[i] = NULL;
	}
	shapeCount = 0;
}